var Graph = (function () {
    function Graph(element) {

        var self = this;
		
		self.element = element;
		
		var svg = d3.select(self.element).select("svg");
		if (svg) svg.remove();

        self.margin = {
            top: 20,
            right: 20,
            bottom: 30,
            left: 100
        };

        self.width = 1600 - self.margin.left - self.margin.right;
        self.height = 600 - self.margin.top - self.margin.bottom;

        self.x = d3.scale.linear()
            .range([0, self.width]);

        self.y = d3.scale.linear()
            .range([self.height, 0]);

        self.xAxis = d3.svg.axis()
            .scale(self.x)
            .orient("bottom");

        self.yAxis = d3.svg.axis()
            .scale(self.y)
            .orient("left");

        self.svg = d3.select(self.element).append("svg")
            .attr("width", self.width + self.margin.left + self.margin.right)
            .attr("height", self.height + self.margin.top + self.margin.bottom)
            .append("g")
            .attr("transform", "translate(" + self.margin.left + "," + self.margin.top + ")");
        self.draw = function (data, xSelector, ySelector) {

            self.x.domain([0, data.length - 1]);
            self.y.domain(d3.extent(data, function (d) {
                return d;
            }));

            self.svg.append("g")
                .attr("class", "y axis")
                .call(self.yAxis)
                .append("text")
                .attr("transform", "rotate(-90)")
                .attr("y", 6)
                .attr("dy", ".71em")
                .style("text-anchor", "end")
                .text("nb attemps");

            var line = d3.svg.line()
                .x(function (obj, index) {
                return xSelector(obj, index, self.x);
            })
                .y(function (obj, index) {
                return ySelector(obj, index, self.y);
            });

            self.svg.append("path")
                .datum(data)
                .attr("class", "line")
                .attr("d", line);
        };
    }

    return Graph;
})();
