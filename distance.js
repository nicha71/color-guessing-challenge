var Distance = {
    deadOn: 0,
    closest: 24,
    veryClose: 48,
    close: 96,
    far: 191,
    veryFar: 383,
    lightYears: 765
};
