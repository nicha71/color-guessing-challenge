var RND = (function () {
    function RND(seed) {

		Math.seedrandom(seed);
	
        var self = this;

        self.seed = seed;
        self.next = function () {
            return Math.random();
        };
        self.range = function (min, max) {
            return Math.floor(this.next() * (max - min)) + min;

        };
    }
    return RND;
})();
