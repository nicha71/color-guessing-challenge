var RGB = (function () {
	
	function RGB(random) {
	
		var self = this;
		
        self.random = random;

        self.red = random.range(0, 256);
        self.green = random.range(0, 256);
        self.blue = random.range(0, 256);

        self.attemptsCount = 0;

        self.test = function (testRgb) {

            if (++self.attemptsCount > (256 * 256 * 256)) {
                throw 'FAIL : Color have not been found in ' + (256 * 256 * 256) + ' attemps. A dumb brute force would have found it by now...';
            }

            var gap = Math.abs(self.red - testRgb.red) + Math.abs(self.green - testRgb.green) + Math.abs(this.blue - testRgb.blue);
            if (gap === 0) return Distance.deadOn;
            else if (gap <= Distance.closest) return Distance.closest;
            else if (gap <= Distance.veryClose) return Distance.veryClose;
            else if (gap <= Distance.close) return Distance.close;
            else if (gap <= Distance.far) return Distance.far;
            else if (gap <= Distance.veryFar) return Distance.veryFar;
            else return Distance.lightYears;
        };
    }

    return RGB;
})();
