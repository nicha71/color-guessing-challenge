var App = (function(){
	App = function(){
	
		var self = this;
	
		self.init = function(visualisationId, summaryId){
            self.visualisationElement = document.getElementById(visualisationId);
            self.summaryElement = document.getElementById(summaryId);
		};
		
		self.run = function(){
		
			var date = new Date();
			var seed = document.getElementsByName("seed")[0].checked ? date.getTime() : ('' + date.getUTCFullYear() + date.getUTCMonth() + date.getUTCDate());
			var repeat = document.getElementById("repeat").value;

            var debug = document.getElementById("debug-mode").checked;
            var red = document.getElementById("debug-red").value;
            var green = document.getElementById("debug-green").value;
            var blue = document.getElementById("debug-blue").value;

			var benchmark = new Benchmark({
				seed: seed,
				repeat: repeat,

                debug: debug ? 'on' : 'off',
                debugColor: debug ? {red : red, green : green, blue : blue} : null

			});
			
			benchmark.run();

			new Graph(this.visualisationElement)
				.draw(
					benchmark.attemptsResults,
				
					function (obj, index, xScale) {
						return xScale(index);
					},
				
					function (obj, index, yScale) {
						return yScale(obj);
					}
				);
			this.summaryElement.innerText = benchmark.summary();	
		};	
	};
	
	return App;
})();
