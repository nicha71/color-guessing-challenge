var Brain = (function () {
    Brain = function (rgb) {

        var self = this;
        self.rgb = rgb;

        self.resolve = function (rgb) {
            var found = false;
            for (var r = 0; r < 256 && !found; r++) {
                for (var g = 0; g < 256 && !found; g++) {
                    for (var b = 0; b < 256 && !found; b++) {
                        var tryColor = {
                            red: r,
                            green: g,
                            blue: b
                        };
                        if (self.rgb.test(tryColor) === Distance.deadOn) {
                            found = true;
                            return tryColor;
                        }
                    }
                }
            }
        };
    };
    return Brain;
})();
