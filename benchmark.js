var Benchmark = (function () {
    Benchmark = function (config) {

        var self = this;

        self.config = config;

        self.run = function () {
            console.clear();

            self.attemptsResults = [];
            self.timingsResults = [];
            self.cumulativeResults = [];

            self.random = new RND(self.config.seed);
			
            for (var i = 0; i < self.config.repeat; i++) {

                var start = performance.now();

                var rgb = new RGB(self.random);

                if(self.config.debug === 'on'){
                    rgb.red = config.debugColor.red;
                    rgb.green = config.debugColor.green;
                    rgb.blue = config.debugColor.blue;
                }

                self.brain = new Brain(rgb);

                try{
                    var resolvedColor = self.brain.resolve();
                }
                finally{
                    console.debug(rgb);
                    console.debug(self.brain);
                }


                var end = performance.now();

                if(rgb.test(resolvedColor) !== 0){
                    throw "ERROR : Brain did not found the right color.";
                }

                self.attemptsResults.push(rgb.attemptsCount);
                self.timingsResults.push((end - start));
                self.cumulativeResults.push({
                    attemptsMean: ss.mean(self.attemptsResults),
                    attemptsSd: ss.standard_deviation(self.attemptsResults),
                    timingMean: ss.mean(self.timingsResults),
                    timingSd: ss.standard_deviation(self.timingsResults)
                });
            }
            self.summaryResults = self.cumulativeResults[self.cumulativeResults.length - 1];
        };

        self.summary = function () {
            return self.config.repeat + ' executions in ' + Math.round(self.config.repeat * self.summaryResults.timingMean) + ' ms \n\n\
Number of attempts : \n\
    Average :             ' + Math.round(self.summaryResults.attemptsMean) + '\n\
    Standard derivation : ' + Math.round(self.summaryResults.attemptsSd) + '\n\n\
Execution time (ms) : \n\
    Average :             ' + Math.round(self.summaryResults.timingMean) + '\n\
    Standard derivation : ' + Math.round(self.summaryResults.timingSd);
        };
    };
    return Benchmark;
})();
